Template.users_dashboard.helpers({
  settings: function() {
    return {
      collection: 'all-users',
      rowsPerPage: 20,
      showFilter: true,
      fields: [
        { key: 'avatar', label: '', tmpl: Template.users_list_avatar, sortable: false },
        { key: 'createdAt', label: 'Tham gia từ', tmpl: Template.users_list_created_at, sort: 'descending' },
        { key: 'isAdmin', label: 'Admin', fn: function(val){return val ? 'Đúng':'Không'} },
        { key: 'username', label: 'Tên tài khoản', tmpl: Template.users_list_username },
        { key: 'telescope.displayName', label: 'Tên hiển thị', tmpl: Template.users_list_display_name },
        { key: 'telescope.email', label: 'Email', tmpl: Template.users_list_email },
        { key: 'telescope.postCount', label: 'Sô bài đăng' },
        { key: 'telescope.commentCount', label: 'Số bình luận' },
        { key: 'telescope.karma', label: 'Số điểm', fn: function(val){return Math.round(100*val)/100} },
        { key: 'telescope.inviteCount', label: 'Đã mời' },
        { key: 'telescope.isInvited', label: 'Được mời', fn: function(val){return val ? 'Đúng':'Không'} },
        { key: 'actions', label: 'Hành động', tmpl: Template.users_list_actions, sortable: false }
      ]
    };
  }
});
